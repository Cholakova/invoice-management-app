import React, { Component } from 'react';
import axios from '../../axios';

import classes from './InvoiceTable.module.css';

import Invoice from '../../components/Invoice/Invoice';
import FullInvoice from '../../components/FullInvoice/FullInvoice';
import NewInvoice from '../../components/NewInvoice/NewInvoice';
import UpdateInvoice from '../../components/UpdateInvoice/UpdateInvoice';
import Modal from '../../components/UI/Modal/Modal';
import Button from '../../components/UI/Button/Button';
import TextFieldGroup from '../../components/UI/TextFieldGroup/TextFieldGroup';

class InvoicesTable extends Component {

    state = {
        headers: ['Date', 'Billing Company', 'Terms', 'Actions'],
        allInvoices: [],
        invoices: [],
        selectedInvoiceId: null,
        updatedInvoice: {},
        error: false,
        billToCompanyFilter: '',
        isFullInvoiceModalShown: false,
        isNewInvoiceModalShown: false,
        isUpdateInvoiceModalShown: false,
        isSortDesc: true
    }

    componentDidMount () {
        // try {
        //     setInterval(async () => {
        //         axios.get( '/invoices' )
                    // .then( response => {
                    //     this.setState({invoices: response.data, allInvoices: response.data});
                    //     console.log( response.data );
                    // } )
                    // .catch(error => {
                    //     console.log(error);
                    // });
        //     }, 300000); / 5min

        //   } catch(e) {
        //     console.log(e);
        //   }

        this.fetchInvoices();
    }

    fetchInvoices = () => {
        axios.get( '/invoices' )
            .then( response => {
                this.setState({invoices: response.data, allInvoices: response.data});
                // console.log( response.data );
            } )
            .catch(error => {
                this.setState({error: true});
                console.log(error);
            });
    }

    // Render the header of the table
    renderTableHeader() {
        return this.state.headers.map((key, index) => {
           return <th key={index}>{key.toUpperCase()}</th>
        })
     }

    // View Full Invoice info
    viewFullInvoiceHandler = (id) => {
        this.setState({selectedInvoiceId: id, isFullInvoiceModalShown: true});
    }

    // Show modal for creating a new invoice
    viewNewInvoiceHandler = () => {
        this.setState({isNewInvoiceModalShown: true})
    }

    // Fetch invoices from the api on new invoice created
    handleNewInvoiceCreated = () => { 
        this.fetchInvoices();
    }

    // Set updated invoice and show the modal for editing
    updateInvoiceHandler = (id) => {
        const invoices = [...this.state.allInvoices];
        let updatedInvoice = invoices.find( invoice => {
            return invoice.id === id
        })

        this.setState({updatedInvoice: updatedInvoice, isUpdateInvoiceModalShown: true});
    }

    // Update the invoices after invoice was updated
    handleUpdate = (id, updatedInvoice) => {
        const invoices = [...this.state.allInvoices];

        const invoiceIndex = invoices.findIndex((item => item.id === id));
        invoices[invoiceIndex] = {id: id, ...updatedInvoice};

        this.setState({allInvoices: invoices, invoices: invoices});
    }

    // Delete invoice
    deleteInvoiceHandler = (id) => {
        axios.delete('/invoices/' + id)
            .then(response => {
                console.log(response);
            });

        const invoices = [
            ...this.state.invoices
        ];
        const allInvoices = [
            ...this.state.allInvoices
        ];

        const updatedInvoicesOrigin = allInvoices.filter(invoice => invoice.id !== id);
        const updatedFilteredInvoices = invoices.filter(invoice => invoice.id !== id);

        this.setState({
            invoices: updatedFilteredInvoices, 
            allInvoices: updatedInvoicesOrigin
        });
    }

    // Sort invoices by Date - ascending and descending
    sortInvoicesHandler = () => {
        const invoices = [...this.state.invoices];
        const isSortDesc = this.state.isSortDesc;
        
        const sortedInvoices = invoices.sort((a,b) => { 
            return new Date(b.date).getTime() - new Date(a.date).getTime()
        });

        if(!isSortDesc) {
            sortedInvoices.reverse();
        }

        this.setState({invoices: sortedInvoices, isSortDesc: !isSortDesc});
    }

    // Handle input change
    handleChange = (e) => {
        
        const allInvoices = [...this.state.allInvoices];
        let filteredInvoices = [];

        if(e.target.value !== '') {
            filteredInvoices = allInvoices.filter(invoice => {
                const lc = invoice.billTo.companyName.toLowerCase();
                const filter = e.target.value.toLowerCase();
                return lc.includes(filter);
            })
        }
        else {
            // If the search bar is empty, set filteredInvoices to original invoices list
            filteredInvoices = allInvoices;
        }

        this.setState({
            invoices: filteredInvoices,
            billToCompanyFilter: e.target.value
        });
    }

    // Close modal
    modalCancelHandler = (modal) => { 
        switch(modal) {
            case 'isFullInvoiceModalShown': {
                this.setState( { isFullInvoiceModalShown: false } );
                break;
            }
            case 'isNewInvoiceModalShown': {
                this.setState( { isNewInvoiceModalShown: false } );
                break;
            }
            case 'isUpdateInvoiceModalShown': {
                this.setState( { isUpdateInvoiceModalShown: false } );
                break;
            }
            default: break;
        }
    }

    render () { 

        let invoices = <p style={{textAlign: 'center'}}>Something went wrong!</p>;

        if (!this.state.error) {
            invoices = this.state.invoices.map(invoice => {
                return <Invoice 
                    key={invoice.id} 
                    id={invoice.id} 
                    date={invoice.date}
                    billTo={invoice.billTo.companyName}
                    terms={invoice.billTo.terms} 
                    viewMore={() => this.viewFullInvoiceHandler(invoice.id)}
                    edit={() => this.updateInvoiceHandler(invoice.id)}
                    delete={() => this.deleteInvoiceHandler(invoice.id)} />;
            });
        }

        return (
            <section className={classes.Card}>
                <div className='clearfix'>
                    <div className='float-left mt-10'>
                        <Button 
                            btnType='Secondary' 
                            clicked={this.sortInvoicesHandler}
                            disabled={false}>
                                SORT
                        </Button>

                        <Button 
                            btnType='Success' 
                            clicked={this.viewNewInvoiceHandler}
                            disabled={false}>
                                NEW
                        </Button>
                    </div>
                    <div className='float-right'>
                        <TextFieldGroup
                            field='filter'
                            placeholder='SEARCH'
                            value={this.state.billToCompanyFilter}
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
                <table className={classes.Table}>
                    <thead>
                        <tr>{this.renderTableHeader()}</tr>
                    </thead>
                    <tbody>
                        {invoices}
                    </tbody>
                </table>
                <Modal show={this.state.isFullInvoiceModalShown} modalClosed={this.modalCancelHandler.bind(this,'isFullInvoiceModalShown')}>
                    <FullInvoice id={this.state.selectedInvoiceId} />
                </Modal>
                <Modal show={this.state.isNewInvoiceModalShown} modalClosed={this.modalCancelHandler.bind(this,'isNewInvoiceModalShown')}>
                    <NewInvoice 
                        onCreateNewInvoice={this.handleNewInvoiceCreated}
                        onCloseModal={this.modalCancelHandler}
                    />                
                </Modal>
                <Modal show={this.state.isUpdateInvoiceModalShown} modalClosed={this.modalCancelHandler.bind(this,'isUpdateInvoiceModalShown')}>
                    <UpdateInvoice 
                        updatedInvoice={this.state.updatedInvoice}
                        onEditInvoice={this.handleUpdate}
                        onCloseModal={this.modalCancelHandler}
                    />                
                </Modal>
            </section>
        );
    }
}

export default InvoicesTable;
