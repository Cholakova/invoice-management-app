import React, { Component } from 'react';

import InvoicesTable from '../InvoicesTable/InvoicesTable';

class Home extends Component {

    render () {

        return (
            <main>
                <h1>Invoices</h1>
                <InvoicesTable />
            </main>
        );
    }
}

export default Home;
