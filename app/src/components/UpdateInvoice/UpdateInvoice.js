import React, { Component } from 'react';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import axios from '../../axios';
import TextFieldGroup from '../UI/TextFieldGroup/TextFieldGroup';
import InvoiceDataForm from '../InvoiceDataForm/InvoiceDataForm';
import Button from '../UI/Button/Button';

class UpdateInvoice extends Component {
    
    constructor(props) {
        super(props);
    
        this.state = {
            data: {
                id: '',
                date: new Date(),
                customerId: '',
                terms: '',
                name: '',
                companyName: '',
                streetAddress: '',
                city: '',
                state: '',
                ZIP: '',
                phone: '',
                email: '',
            },
            invoiceData: [
                {
                    description: '',
                    qty: '',
                    unitPrice: '',
                    amount: ''
                }
            ]
        }
    }

    componentWillReceiveProps(props) {
        const {billTo, date, id, invoiceData} = props.updatedInvoice;
        
        const data = {
            id: id,
            date: moment(date).toDate(),
            ...billTo
        }

        this.setState({data: data, invoiceData: invoiceData});
    }
    
    onSubmitHandler = (event) => { 
        event.preventDefault(); 
        const { data, invoiceData } = this.state;
        const postData = {
            date: moment(data.date).format('MM/DD/YYYY'),
            billTo: {
                customerId: data.customerId,
                terms: data.terms,
                name: data.name,
                companyName: data.companyName,
                streetAddress: data.streetAddress,
                city: data.city,
                state: data.state,
                ZIP: data.ZIP,
                phone: data.phone,
                email: data.email
            },
            invoiceData: invoiceData
        };

        axios.put('/invoices/' + this.state.data.id, postData)
            .then(response => {
                console.log(response);
                this.props.onEditInvoice(this.state.data.id, postData);
            });

        this.resetForm();
        this.props.onCloseModal('isUpdateInvoiceModalShown');
    }

    resetForm = () => {
        this.formRef.reset();
        this.setState({ 
            data: {
                id: '',
                date: new Date(),
                customerId: '',
                terms: '',
                name: '',
                companyName: '',
                streetAddress: '',
                city: '',
                state: '',
                ZIP: '',
                phone: '',
                email: '',
            },
            invoiceData: [
                {
                    description: '',
                    qty: '',
                    unitPrice: '',
                    amount: ''
                }
            ]
         })
    }

    onCancelHandler = () => {
        this.resetForm();
        this.props.onCloseModal('isUpdateInvoiceModalShown');
    }

    handleInputChange (e) { 
        this.setState({ data: {...this.state.data, [e.target.name]: e.target.value} })
    }

    handleInputInvoiceDataChange (e) { 
        const invoiceData = [...this.state.invoiceData];
        invoiceData[e.target.dataset.id][e.target.dataset.name] = e.target.value;
        this.setState({ invoiceData: invoiceData });
    }

    handleDate(date) {
        this.setState( { data: { ...this.state.data, date: date }} )
       }

    addInvoiceDataSubForm = (e) => { 
        this.setState((prevState) => ({
          invoiceData: [...prevState.invoiceData, { description: '', qty: '', unitPrice: '', amount: ''}],
        }));
    }

    render () {

        const { data, invoiceData }  = this.state;

        return (
            <div>
                <h1>Update Invoice</h1>
                <form ref={(el) => this.formRef = el} onSubmit={this.onSubmitHandler}>
                    <div>
                        <DatePicker
                            selected={data.date || moment() }
                            name='date'
                            onChange={this.handleDate.bind(this)}
                        />

                        <h3>Bill to</h3>
                        <TextFieldGroup
                            field='customerId'
                            label='Customer Id'
                            value={ data.customerId || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='terms'
                            label='Terms'
                            value={ data.terms || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='name'
                            label='Name'
                            value={ data.name || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='companyName'
                            label='Company Name'
                            value={ data.companyName || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='streetAddress'
                            label='Street Address'
                            value={ data.streetAddress || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='city'
                            label='City'
                            value={ data.city || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='state'
                            label='State'
                            value={ data.state || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='ZIP'
                            label='ZIP'
                            value={ data.ZIP || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='phone'
                            label='Phone'
                            value={ data.phone || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='email'
                            label='Email'
                            type='email'
                            value={ data.email || '' }
                            onChange={this.handleInputChange.bind(this)}
                        />
                    </div>
                    <div>
                        <InvoiceDataForm 
                            invoiceData={invoiceData || []}
                            change={this.handleInputInvoiceDataChange.bind(this)} />

                        <Button 
                            btnType='Primary' 
                            clicked={this.addInvoiceDataSubForm}
                            disabled={false}>
                                Add new Invoice Data
                        </Button>
                    </div>
                    <div className='float-right'>
                        <Button 
                            btnType='Secondary' 
                            clicked={this.onCancelHandler}
                            disabled={false}>
                                Cancel
                        </Button>
                        <Button 
                            btnType='Success' 
                            type='submit'
                            disabled={false}>
                                Save
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
    
}

export default UpdateInvoice;