import React from 'react';
import Button from '../UI/Button/Button';

const invoice = (props) => (
    <tr key={props.id}>
        <td>{props.date}</td>
        <td>{props.billTo}</td>
        <td>{props.terms}</td>
        <td>
            <Button 
                btnType="Primary" 
                className='Small'
                clicked={props.viewMore}
                disabled={false}>
                    VIEW
            </Button>
            <Button 
                btnType="Success" 
                className='Small'
                clicked={props.edit}
                disabled={false}>
                    EDIT
            </Button>
            <Button 
                btnType="Danger" 
                className='Small'
                clicked={props.delete}
                disabled={false}>
                    DEL
            </Button>
        </td>
    </tr>
);

export default invoice;
