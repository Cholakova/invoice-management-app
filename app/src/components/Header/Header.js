import React from 'react';

import classes from './Header.module.css';

function Header() {
    return (
        <div className={classes.Header}>
            <p>Invoice Manager</p>
        </div>
    );
}

export default Header;
