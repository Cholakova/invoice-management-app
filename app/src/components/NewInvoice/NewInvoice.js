import React, { Component } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import axios from '../../axios';
import InvoiceDataForm from '../InvoiceDataForm/InvoiceDataForm';
import TextFieldGroup from '../UI/TextFieldGroup/TextFieldGroup';
import Button from '../UI/Button/Button';

class NewInvoice extends Component {

    state = {
        data: {
            id: '',
            date: new Date(),
            customerId: '',
            terms: '',
            name: '',
            companyName: '',
            streetAddress: '',
            city: '',
            state: '',
            ZIP: '',
            phone: '',
            email: '',
        },
        invoiceData: [
            {
                description: '',
                qty: '',
                unitPrice: '',
                amount: ''
            }
        ],
        errors: {},
        formIsSubmitting: false
    }

    onSubmitHandler = (event) => { 
        event.preventDefault();
        if (this.state.formIsSubmitting === true) {
            return;
        }

        if (this.validateSubmit()) {
            this.setState({ formIsSubmitting: true, errors: {} }, () => {
                const { data, invoiceData } = this.state;
                const postData = {
                    date: moment(data.date).format('MM/DD/YYYY'),
                    billTo: {
                        customerId: data.customerId,
                        terms: data.terms,
                        name: data.name,
                        companyName: data.companyName,
                        streetAddress: data.streetAddress,
                        city: data.city,
                        state: data.state,
                        ZIP: data.ZIP,
                        phone: data.phone,
                        email: data.email
                    },
                    invoiceData: invoiceData
                };

                axios.post('/invoices', postData)
                    .then(response => {
                        console.log(response);
                        this.props.onCreateNewInvoice();
                    });

                this.resetForm();
                this.props.onCloseModal('isNewInvoiceModalShown');
            })
        }
        else { console.log('error')
            document.getElementById('form').scrollIntoView();
        }
    }

    validateSubmit = () => {
        let errors = {}
        const { data } = this.state
        if (data.customerId.length === 0) {
          errors['customerId'] = 'Required'
        }
        if (data.terms.length === 0) {
          errors['terms'] = 'Required'
        }
        if (data.name.length === 0) {
          errors['name'] = 'Required'
        }
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(data.email))) {
            errors['email'] = 'Invalid email'
        }

    
        if (Object.keys(errors).length > 0) {
          this.setState({ errors: errors })
          return false;
        }
        return true;
    }

    resetForm = () => {
        this.formRef.reset();
        this.setState({ 
            data: {
                id: '',
                date: new Date(),
                customerId: '',
                terms: '',
                name: '',
                companyName: '',
                streetAddress: '',
                city: '',
                state: '',
                ZIP: '',
                phone: '',
                email: '',
            },
            invoiceData: [
                {
                    description: '',
                    qty: '',
                    unitPrice: '',
                    amount: ''
                }
            ],
            errors: {},
            formIsSubmitting: false
         })
    }

    onCancelHandler = () => {
        this.resetForm();
        this.props.onCloseModal('isNewInvoiceModalShown');
    }

    handleInputChange (e) { 
        this.setState({ data: {...this.state.data, [e.target.name]: e.target.value} })
    }

    handleInputInvoiceDataChange (e) {
        const invoiceData = [...this.state.invoiceData];
        invoiceData[e.target.dataset.id][e.target.dataset.name] = e.target.value;
        this.setState({ invoiceData });
    }

    handleDate(date) {
        this.setState( { data: { ...this.state.data, date: date }} )
       }

    addInvoiceDataSubForm = (e) => { 
        this.setState((prevState) => ({
          invoiceData: [...prevState.invoiceData, { description: '', qty: '', unitPrice: '', amount: ''}],
        }));
    }

    render () {

        const { data, invoiceData, errors }  = this.state;

        return (
            <div>
                <h1>New Invoice</h1>
                <form ref={(el) => this.formRef = el} id='form' onSubmit={this.onSubmitHandler}>
                    <div>
                        <DatePicker
                            selected={data.date}
                            name='date'
                            onChange={this.handleDate.bind(this)}
                        />

                        <h3>Bill to</h3>
                        <TextFieldGroup
                            field='customerId'
                            label='Customer Id'
                            value={ data.customerId }
                            error={ errors.customerId }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='terms'
                            label='Terms'
                            value={ data.terms }
                            error={ errors.terms }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='name'
                            label='Name'
                            value={ data.name }
                            error={ errors.name }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='companyName'
                            label='Company Name'
                            value={ data.companyName }
                            error={ errors.companyName }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='streetAddress'
                            label='Street Address'
                            value={ data.streetAddress }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='city'
                            label='City'
                            value={ data.city }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='state'
                            label='State'
                            value={ data.state }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='ZIP'
                            label='ZIP'
                            value={ data.ZIP }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='phone'
                            label='Phone'
                            value={ data.phone }
                            onChange={this.handleInputChange.bind(this)}
                        />
                        <TextFieldGroup
                            field='email'
                            label='Email'
                            type='email'
                            value={ data.email }
                            error={ errors.email }
                            onChange={this.handleInputChange.bind(this)}
                        />
                    </div>
                    <div>
                        <InvoiceDataForm 
                            invoiceData={invoiceData}
                            change={this.handleInputInvoiceDataChange.bind(this)} />

                        <Button 
                            btnType='Primary' 
                            clicked={this.addInvoiceDataSubForm}
                            disabled={false}>
                                Add new Invoice Data
                        </Button>
                    </div>
                    <div className='float-right'>
                        <Button 
                            btnType='Secondary' 
                            clicked={this.onCancelHandler}
                            disabled={false}>
                                Cancel
                        </Button>
                        <Button 
                            btnType='Success' 
                            type='submit'
                            disabled={false}>
                                Create
                        </Button>
                    </div>
                </form>
            </div>
        );
    }
    
}

export default NewInvoice;
