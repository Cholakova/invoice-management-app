import React from 'react';
import classes from './InvoiceData.module.css';

const InvoiceData = (props) => {
    return (
        <div className={classes.InvoiceDataWrapper}>
            <p>
                <b>Description: </b>{props.description}
            </p>
            <p>
                <b>Quantity: </b>{props.qty}
            </p>
            <p>
                <b>Unit Price: </b>{props.unitPrice}
            </p>
            <p>
                <b>Amount: </b>{props.amount}
            </p>
        </div>
        
    )
}

export default InvoiceData;
