import React from 'react';

import classes from './Button.module.css';

const button = (props) => (
    <button
        type={props.type ? props.type : 'button'}
        disabled={props.disabled}
        className={[classes.Button, classes[props.btnType], classes[props.className]].join(' ')}
        onClick={props.clicked}>{props.children}</button>
);

export default button;
