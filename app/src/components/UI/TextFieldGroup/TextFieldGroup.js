import React from 'react';

import classes from './TextFieldGroup.module.css';

const TextFieldGroup = ({ field, value, label, placeholder, error, type, onChange, dataId, dataName }) => {
    return (
      <div className={`${classes.Input} ${error ? classes.Invalid : ''}`}>
        <label className={classes.Label} >{label}</label>
        <div className=''>
          <input
            placeholder={placeholder}
            type={type}
            name={field}
            data-id={dataId}
            data-name={dataName}
            className={classes.InputElement}
            onChange={onChange}
            value={value}
          />
          {error && <span className={classes.HelpBlock}>{error}</span>}
        </div>
      </div>
    );
  }

export default TextFieldGroup;
