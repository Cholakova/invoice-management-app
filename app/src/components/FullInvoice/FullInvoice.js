import React, { Component } from 'react';
import axios from '../../axios';

import InvoiceData from '../../components/InvoiceData/InvoiceData';
import classes from './FullInvoice.module.css';


class FullInvoice extends Component {
    state = {
        loadedInvoice: null
    }

    componentDidUpdate () {
        if ( this.props.id ) { 
            if ( !this.state.loadedInvoice || (this.state.loadedInvoice && this.state.loadedInvoice.id !== this.props.id) ) {
                axios.get( '/invoices/' + this.props.id )
                    .then( response => {
                        this.setState( { loadedInvoice: response.data } );
                    } );
            }
        }
    }

    render () {
        let invoice = <p></p>;
        if ( this.props.id ) { 
            invoice = <p style={{ textAlign: 'center' }}>Loading...!</p>;
        }
        if ( this.state.loadedInvoice ) {
            const { billTo, date, id, invoiceData } = this.state.loadedInvoice;

            const invoiceDataList = invoiceData.map((item,index) => {
                return <InvoiceData 
                    key={index}
                    description={item.description}
                    qty={item.qty}
                    unitPrice={item.unitPrice}
                    amount={item.amount}
                    />;
            });

            invoice = (
                <div className={classes.FullInvoice}>
                    <div className={classes.InvoiceInfoWrapper}>
                        <p><b>ID: </b>{id}</p>
                        <p><b>Date: </b>{date}</p>
                    </div>
                    <div className={classes.BillToInfoWrapper}>
                            <h3>Bill to</h3>
                            <div className={classes.Clearfix}>
                                <div className={classes.HalfBox}>
                                    <p><b>Customer id: </b> {billTo.customerId}</p>
                                    <p><b>Terms: </b> {billTo.terms}</p>
                                    <p><b>Name: </b> {billTo.name}</p>
                                    <p><b>Company Name: </b> {billTo.companyName}</p>
                                </div>
                                <div className={classes.HalfBox}>
                                    <p><b>Street Address: </b> {billTo.streetAddress}</p>
                                    <p><b>City: </b> {billTo.city}</p>
                                    <p><b>State: </b> {billTo.state}</p>
                                    <p><b>ZIP: </b> {billTo.zip}</p>
                                    <p><b>Phone: </b> {billTo.phone}</p>
                                    <p><b>Email: </b> {billTo.email}</p>
                                </div>
                            </div>
                        </div>
                    <div className={classes.InvoiceDataWrapper}>
                        {invoiceDataList}
                    </div>
                </div>

            );
        }
        return invoice;
    }
}

export default FullInvoice;
