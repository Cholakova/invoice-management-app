import React from 'react';
import TextFieldGroup from '../UI/TextFieldGroup/TextFieldGroup';

import classes from './InvoiceDataForm.module.css';

const InvoiceDataForm = (props) => { 
    return (
        props.invoiceData.map((val, idx)=> {
            let descId = `desc-${idx}`, 
                qtyId = `qty-${idx}`,
                unitPriceId = `unitPrice-${idx}`,
                amountId = `amount-${idx}`
            return (
                <div key={idx} className={classes.InvoiceData}>
                <h3>Invoice Data {idx + 1}</h3>
                <TextFieldGroup
                    field={descId}
                    label='Description'
                    value={ props.invoiceData[idx].description || '' }
                    dataId={idx}
                    dataName='description'
                    onChange={props.change}
                />
                <TextFieldGroup
                    field={qtyId}
                    label='Quantity'
                    value={ props.invoiceData[idx].qty || '' }
                    dataId={idx}
                    dataName='qty'
                    onChange={props.change}
                />
                <TextFieldGroup
                    field={unitPriceId}
                    label='Unit Price'
                    value={ props.invoiceData[idx].unitPrice || '' }
                    dataId={idx}
                    dataName='unitPrice'
                    onChange={props.change}
                />
                <TextFieldGroup
                    field={amountId}
                    label='Amount'
                    value={ props.invoiceData[idx].amount || '' }
                    dataId={idx}
                    dataName='amount'
                    onChange={props.change}
                />
              </div>
            )
          })
    )
}

export default InvoiceDataForm;
