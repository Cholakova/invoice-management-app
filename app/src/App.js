import React from 'react';
import Header from './components/Header/Header';
import Home from './containers/Home/Home';

function App() {
  return (
    <div>
      <Header />
      <Home />
    </div>
  );
}

export default App;
